<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;
require '../src/Email.php';

final class EmailTest extends TestCase {

    public function test_can_be_created_from_valid_address(): void {
       $this->assertInstanceOf(
         Email::class,
         Email::fromString('user@example.com'),
         "Entered email address does not produce an Email instance"
       );
    }

    public function test_cannot_be_created_from_invalid_email_address(): void {
       $this->expectException(InvalidArgumentException::class);
       Email::fromString('invalid');
    }
}